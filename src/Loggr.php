<?php

namespace jBoy\Loggr;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use jBoy\Loggr\Model\LoggrModel as LoggrModel;
use Carbon\Carbon;

class Loggr{

	/**
	 * Log activity to db.
	 *
	 * @param $userId
	 * @param string $text
	 * @param array  $ip
	 *
	 * @return void
	 */
	public function loggr($text, $type){
		LoggrModel::create(array(
		  'type' => $type == null ? 'info' : $type,
		  'text' => $text,
		  'user_id' => Auth::user()->id,
		  'ip' => Request::ip(),
		));
	}


	/**
	 * Clean up old logs
	 *
	 * @param int $maxAgeInMonths
	 * @return void
	 */
	public function cleanLog($maxDays){
		$maxDays = Config::get('loggr.deleteLogsOlderThanDays');
		$minimumDate = Carbon::now()->subDays($maxDays);
		LoggrModel::where('created_at', '<=', $minimumDate)->delete();
	}
}