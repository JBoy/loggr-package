<?php
namespace jBoy\Loggr;
use Illuminate\Support\Facades\Facade;

class LoggrFacade  extends Facade{

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'loggr';
	}
}