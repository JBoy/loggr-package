<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Max age in days for log records
    |--------------------------------------------------------------------------
    |
    | When running the cleanLog-command all recorder older than the number of days specified here will be deleted
    |
    */
    'deleteLogsOlderThanDays' => 2,
];
