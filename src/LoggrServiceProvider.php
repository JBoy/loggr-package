<?php namespace jBlog\Loggr;

use Illuminate\Support\ServiceProvider;
use jBoy\Loggr\Loggr;

/**
 * A Laravel 5's package template.
 *
 * @author: Johannes 
 */
class LoggrServiceProvider extends ServiceProvider {

    /**
     * This will be used to register config & view in 
     * your package namespace.
     *
     * --> Replace with your package name <--
     */
    protected $packageName = 'loggr';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        // Publish the config file
        $this->publishes([
          __DIR__.'/config/loggr.php' => config_path('loggr.php'),
        ], 'config');

        // Publish the migration file
        $this->publishes([
          __DIR__.'/migrations/create_loggr_table.stub' => base_path('/database/migrations/'.date('Y_m_d_His', time()) .'_create_loggr_table.php'),
        ], 'migrations');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path($this->packageName.'.php'),
        ]);
		$this->app->bind('loggr', function ($app) {
			return new Loggr();
		});

        //
    }

}
